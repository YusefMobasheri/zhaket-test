<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_message}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m191008_083413_create_user_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_message}}', [
            'id' => $this->primaryKey(),
            'user_id_from' => $this->integer(11)->notNull(),
            'user_id_to' => $this->integer(11)->notNull(),
            'message' => $this->string()->notNull(),
            'seen' => $this->tinyInteger(1)->defaultValue(0),
            'date' => $this->integer(12),
        ],'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        // creates index for column `user_id_from`
        $this->createIndex(
            '{{%idx-user_message-user_id_from}}',
            '{{%user_message}}',
            'user_id_from'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-user_message-user_id_from}}',
            '{{%user_message}}',
            'user_id_from',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id_to`
        $this->createIndex(
            '{{%idx-user_message-user_id_to}}',
            '{{%user_message}}',
            'user_id_to'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-user_message-user_id_to}}',
            '{{%user_message}}',
            'user_id_to',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-user_message-user_id_from}}',
            '{{%user_message}}'
        );

        // drops index for column `user_id_from`
        $this->dropIndex(
            '{{%idx-user_message-user_id_from}}',
            '{{%user_message}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-user_message-user_id_to}}',
            '{{%user_message}}'
        );

        // drops index for column `user_id_to`
        $this->dropIndex(
            '{{%idx-user_message-user_id_to}}',
            '{{%user_message}}'
        );

        $this->dropTable('{{%user_message}}');
    }
}
