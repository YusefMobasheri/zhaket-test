<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $mobile
 * @property int $create_date
 *
 * @property UserMessage[] $userMessages
 * @property UserMessage[] $userMessages0
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name'], 'required'],
            [['create_date'], 'integer'],
            [['first_name', 'last_name'], 'string', 'max' => 50],
            [['mobile'], 'string', 'max' => 12],
            [['create_date'], 'default', 'value' => time()],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'mobile' => 'Mobile',
            'create_date' => 'Create Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(UserMessage::className(), ['user_id_from' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSentMessages()
    {
        return $this->hasMany(UserMessage::className(), ['user_id_to' => 'id']);
    }

    public function fields()
    {
        return ['first_name', 'last_name', 'mobile', 'messages'];
    }

    /**
     * Returns user full name
     * @return string
     */
    public function getFullname()
    {
        return "$this->first_name $this->last_name";
    }
}
