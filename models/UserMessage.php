<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_message".
 *
 * @property int $id
 * @property int $user_id_from
 * @property int $user_id_to
 * @property string $message
 * @property int $seen
 * @property int $date
 *
 * @property User $userIdFrom
 * @property User $userIdTo
 */
class UserMessage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id_from', 'user_id_to', 'message'], 'required'],
            [['user_id_from', 'user_id_to', 'seen', 'date'], 'integer'],
            [['message'], 'string', 'max' => 255],
            [['user_id_from'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id_from' => 'id']],
            [['user_id_to'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id_to' => 'id']],
            [['date'], 'default', 'value' => time()],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id_from' => 'User Id From',
            'user_id_to' => 'User Id To',
            'message' => 'Message',
            'seen' => 'Seen',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserIdFrom()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserIdTo()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id_to']);
    }

    public function fields()
    {
        return [
            'from' => function ($model) {
                return $model->userIdFrom?$model->userIdFrom->fullname:'Deleted';
            },
            'message',
            'date' => function ($model) {
                return date('Y/m/d', $model->date);
            }
        ];
    }
}
