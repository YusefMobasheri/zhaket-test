<?php


namespace app\controllers;


use app\models\User;
use yii\rest\ActiveController;
use yii\web\HttpException;

class UserController extends ActiveController
{
    public $modelClass = 'app\models\User';
}